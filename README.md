There are many ways to automate our API testing. One of them uses the Mocha and Chai test framework.

Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, making asynchronous testing simple and fun. 
Mocha tests run serially, allowing for flexible and accurate reporting, while mapping uncaught exceptions to the correct test cases.

Chai is a BDD / TDD assertion library for node and the browser that can be delightfully paired with any javascript testing framework.

This project is a simple project that represents an example of an API Automation test project using Mocha Chai.
The API used as an example is from open source API like:
- https://apipheny.io/free-api/
- https://github.com/keikaavousi/fake-store-api

The project has structure like:
![img.png](structureProject.png)

a. helper
Contains response message and code

b. lib/endpoint
Contains the endpoint files that will be automated

c. lib/scheme
Contains scheme files which is used when asserting the schema of an api response

d. lib/test
Contains test files where to write scenario testing script

e. lib/testdata
Contains testdata files that store test data used in automation test scripts and used as input data

f. reports
Contains reports that are generated when running the test

g. .env
Contains environment variables

h. package.json
Contains dependencies that must be installed

How to run test:
- Using command: npm run test-api