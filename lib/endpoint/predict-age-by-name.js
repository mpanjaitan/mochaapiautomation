const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.PREDICT_AGE_BASE_URL);

/*using reference API https://apipheny.io/free-api/*/
const getAgeByName = (query) => api.get(``)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .query(query);

module.exports = {
    getAgeByName,
}