const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.PRODUCT_BASE_URL);

/*using reference API https://github.com/keikaavousi/fake-store-api/*/
const getProductList = () => api.get(`/products`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

const getSingleProduct = () => api.get(`/products/2`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

const addNewProduct = (body) => api.post(`/products`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(body);

const updateProduct = (body) => api.put(`/products/7`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(body);

const patchProduct = (body) => api.patch(`/products/8`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(body);

const deleteProduct = () => api.delete(`/products/9`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

const sortProduct = (query) => api.get(`/products`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .query(query);

module.exports = {
    getProductList,
    getSingleProduct,
    addNewProduct,
    updateProduct,
    patchProduct,
    deleteProduct,
    sortProduct
}