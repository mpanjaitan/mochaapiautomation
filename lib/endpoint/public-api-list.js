const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.PUBLIC_API_BASE_URL);

/*using reference API https://apipheny.io/free-api/*/
const getPublicAPIList = () => api.get(`/entries`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

module.exports = {
    getPublicAPIList,
}