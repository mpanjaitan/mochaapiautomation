
const assert = require('chai').expect;

const page = require('../endpoint/movie-list-page.js');
const testDataMovie = require('../testdata/movie-data.json');
const responseCode = require('../../helper/responseMessageAndCode.json');

const testCase = {
    "positive" : {
        "getList" : "As a User, I want to be able to get OMDB Movie list",
    },
    "negative" : {
        "noSearch" : "As a User, I should got error message when I send request without key of search",
        "invalidApiKey" : "As a User, I should got error 401 when I send request with invalid API Key"
    }
}

describe(`OMDB Movie List`, () => {
    /*const apiKey = 'cc99fc43';
    const invalidApiKey = 'sdfsdf';
    const keySearch = 'lord';*/

    it(`@get ${testCase.positive.getList}`, async() => {
        const response = await page.getMovieList(testDataMovie.apiKey, testDataMovie.keySearch);
        assert(response.status).to.equal(responseCode.successOk);
    }),

        it(`@get ${testCase.negative.noSearch}`, async() => {
            const response = await page.getMovieList(testDataMovie.apiKey, '');
            assert(response.status).to.equal(responseCode.successOk, response.body.Error);
            assert(response.body.Response).to.equal(testDataMovie.responseFalse);
            assert(response.body.Error).to.equal(testDataMovie.responseIncorrectIMDbID);
        }),

        it(`@get ${testCase.negative.invalidApiKey}`, async() => {
            const response = await page.getMovieList(testDataMovie.invalidAPIKey, testDataMovie.keySearch);
            assert(response.status).to.equal(responseCode.failedUnauthorized.codeNumber, response.body.Error);
            assert(response.body.Response).to.equal(testDataMovie.responseFalse);
            assert(response.body.Error).to.equal(testDataMovie.responseInvalidAPIKey);
        })
})