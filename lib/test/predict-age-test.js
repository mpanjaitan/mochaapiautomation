const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const endpointPredictAge = require('../endpoint/predict-age-by-name.js');
const testDataPredictAge = require('../testdata/predict-age-data.json');
const schemaPredictAge = require('../scheme/predict-age-schema.json');
const responseCode = require('../../helper/responseMessageAndCode.json');

const testCase = {
    "positive" : {
        "predictAgeByNameLisa": "As a User, I want to know predict age by name Lisa",
        "predictAgeByNameLeo": "As a User, I want to know predict age by name Leo",
        "predictAgeByNameNunu": "As a User, I want to know predict age by name Nunu",
        "predictAgeByNameCalista": "As a User, I want to know predict age by name Calista",
    }
}

describe(`Predict Age By Name`, () => {

    it(`@get ${testCase.positive.predictAgeByNameLisa}`, async() => {
        const response = await endpointPredictAge.getAgeByName(testDataPredictAge.nameLisa);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaPredictAge.successGetPredictAgeByNameSchema);
        expect(response.body.name).to.equal(testDataPredictAge.nameLisa.name, testDataPredictAge.errorNameNotMatch);
    })

    it(`@get ${testCase.positive.predictAgeByNameLeo}`, async() => {
        const response = await endpointPredictAge.getAgeByName(testDataPredictAge.nameLeo);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaPredictAge.successGetPredictAgeByNameSchema);
        expect(response.body.name).to.equal(testDataPredictAge.nameLeo.name, testDataPredictAge.errorNameNotMatch);
    })

    it(`@get ${testCase.positive.predictAgeByNameNunu}`, async() => {
        const response = await endpointPredictAge.getAgeByName(testDataPredictAge.nameNunu);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaPredictAge.successGetPredictAgeByNameSchema);
        expect(response.body.name).to.equal(testDataPredictAge.nameNunu.name, testDataPredictAge.errorNameNotMatch);
    })

    it(`@get ${testCase.positive.predictAgeByNameCalista}`, async() => {
        const response = await endpointPredictAge.getAgeByName(testDataPredictAge.nameCalista);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaPredictAge.successGetPredictAgeByNameSchema);
        expect(response.body.name).to.equal(testDataPredictAge.nameCalista.name, testDataPredictAge.errorNameNotMatch);
    })
})