const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const endpointProduct = require('../endpoint/product.js');
const testDataProduct = require('../testdata/product-data.json');
const schemaProduct = require('../scheme/product-schema.json');
const responseCode = require('../../helper/responseMessageAndCode.json');

const testCase = {
    "positive" : {
        "getListAllProduct": "As a User, I want to be able to view all product list",
        "getSingleProduct": "As a User, I want to be able to view specific product",
        "addNewProduct": "As a User, I want to be able to add new product",
        "updateProduct": "As a User, I want to be able to update product",
        "patchProduct": "As a User, I want to be able to update partial information product",
        "deleteProduct": "As a User, I want to be able to delete product",
        "sortingProduct": "As a User, I want to be able to sorting product"
    }
}

describe(`All about product`, () => {

    it(`@get ${testCase.positive.getListAllProduct}`, async() => {
        const response = await endpointProduct.getProductList();
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successGetListProductsSchema);
    })

    it(`@get ${testCase.positive.getSingleProduct}`, async() => {
        const response = await endpointProduct.getSingleProduct();
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successGetSingleProductSchema);
    })

    it(`@get ${testCase.positive.addNewProduct}`, async() => {
        const response = await endpointProduct.addNewProduct(testDataProduct.addNewProduct);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successAddProductSchema);
        expect(response.body.title).to.equal(testDataProduct.addNewProduct.title, JSON.stringify(response.body));
        expect(response.body.price).to.equal(testDataProduct.addNewProduct.price, JSON.stringify(response.body));
        expect(response.body.description).to.equal(testDataProduct.addNewProduct.description, JSON.stringify(response.body));
        expect(response.body.image).to.equal(testDataProduct.addNewProduct.image, JSON.stringify(response.body));
        expect(response.body.category).to.equal(testDataProduct.addNewProduct.category, JSON.stringify(response.body));
    })

    it(`@get ${testCase.positive.updateProduct}`, async() => {
        const response = await endpointProduct.updateProduct(testDataProduct.updateProduct);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successAddProductSchema);
        expect(response.body.title).to.equal(testDataProduct.updateProduct.title, JSON.stringify(response.body));
        expect(response.body.price).to.equal(testDataProduct.updateProduct.price, JSON.stringify(response.body));
        expect(response.body.description).to.equal(testDataProduct.updateProduct.description, JSON.stringify(response.body));
        expect(response.body.image).to.equal(testDataProduct.updateProduct.image, JSON.stringify(response.body));
        expect(response.body.category).to.equal(testDataProduct.updateProduct.category, JSON.stringify(response.body));
    })

    it(`@get ${testCase.positive.patchProduct}`, async() => {
        const response = await endpointProduct.patchProduct(testDataProduct.patchProduct);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successAddProductSchema);
        expect(response.body.title).to.equal(testDataProduct.patchProduct.title, JSON.stringify(response.body));
        expect(response.body.price).to.equal(testDataProduct.patchProduct.price, JSON.stringify(response.body));
        expect(response.body.description).to.equal(testDataProduct.patchProduct.description, JSON.stringify(response.body));
        expect(response.body.image).to.equal(testDataProduct.patchProduct.image, JSON.stringify(response.body));
        expect(response.body.category).to.equal(testDataProduct.patchProduct.category, JSON.stringify(response.body));
    })

    it(`@get ${testCase.positive.deleteProduct}`, async() => {
        const response = await endpointProduct.deleteProduct();
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successGetSingleProductSchema);
    })

    it(`@get ${testCase.positive.sortingProduct}`, async() => {
        const response = await endpointProduct.sortProduct(testDataProduct.sortingProduct);
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaProduct.successGetListProductsSchema);
    })
})