const { expect } = require('chai');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const endpointPublicApi = require('../endpoint/public-api-list.js');
const testDataPublicApi = require('../testdata/public-api-data.json');
const schemaPublicApi = require('../scheme/public-api-schema.json');
const responseCode = require('../../helper/responseMessageAndCode.json');

const testCase = {
    "positive" : {
        "getList": "As a User, I want to be able to get Public API list",
        "validateCount": "As a User, I should know the count"
    }
}

describe(`Public API List`, () => {

    it(`@get ${testCase.positive.getList}`, async() => {
        const response = await endpointPublicApi.getPublicAPIList();
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body).to.be.jsonSchema(schemaPublicApi.successGetListPublicAPISchema);
    })

    it(`@get ${testCase.positive.validateCount}`, async() => {
        const response = await endpointPublicApi.getPublicAPIList();
        expect(response.status).to.equal(responseCode.successOk, JSON.stringify(response.body));
        expect(response.body.count).to.equal(testDataPublicApi.count, testDataPublicApi.messageUnmatchedCount);
    })
})